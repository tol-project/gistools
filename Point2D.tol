  /* -*- mode: C++ -*- */
//////////////////////////////////////////////////////////////////////////////
// FILE   : Point2D.tol
// PURPOSE: Defines Class GisTools::@Point2D and inherited classes
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//Abstract class to handle with a bidimensional point.
Class @Point2D
//////////////////////////////////////////////////////////////////////////////
{
  ////////////////////////////////////////////////////////////////////////////
  Static Text _.autodoc.member.numberOfPoints = 
  "Returns the number of stored points";
  Real numberOfPoints(Real void);
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////
  Static Text _.autodoc.member.x = 
  "Returns the first coordinate of the k-th point";
  Real x(Real k);
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////
  Static Text _.autodoc.member.y = 
  "Returns the second coordinate of the k-th point";
  Real y(Real k)
  ////////////////////////////////////////////////////////////////////////////
};

//////////////////////////////////////////////////////////////////////////////
//Class to handle with just one bidimensional point that
//isn�t externally stored
Class @Point2D.Simple : @Point2D
//////////////////////////////////////////////////////////////////////////////
{
  Real _.x;
  Real _.y;

  //Just one point is stored
  Real numberOfPoints(Real void) { 1 };
  //Argument k is unused
  Real x(Real k) { _.x };
  //Argument k is unused
  Real y(Real k) { _.y };
  ////////////////////////////////////////////////////////////////////////////
  Static Text _.autodoc.member.New = "Creates an instance of @Point2D.Simple";
  Static @Point2D.Simple New(Real x, Real y)
  ////////////////////////////////////////////////////////////////////////////
  {
    @Point2D.Simple new = [[
      Real _.x = x;
      Real _.y = y ]];
    new
  };
  ////////////////////////////////////////////////////////////////////////////
  Static Text _.autodoc.member.FromRow = "Creates an instance of "
  "@Point2D.Simple using the 2 first columns of selected row of given matrix";
  Static @Point2D.Simple FromRow(VMatrix M, Real numRow)
  ////////////////////////////////////////////////////////////////////////////
  {
    @Point2D.Simple new = [[
      Real _.x = VMatDat(M,numRow,1);
      Real _.y = VMatDat(M,numRow,2) ]];
    new
  };
  ////////////////////////////////////////////////////////////////////////////
  Static Text _.autodoc.member.FromCol = "Creates an instance of "
  "@Point2D.Simple using the 2 first rows of selected column of given matrix";
  Static @Point2D.Simple FromCol(VMatrix M, Real numCol)
  ////////////////////////////////////////////////////////////////////////////
  {
    @Point2D.Simple new = [[
      Real _.x = VMatDat(M,1,numCol);
      Real _.y = VMatDat(M,2,numRow) ]];
    new
  }
};

//////////////////////////////////////////////////////////////////////////////
//Class to store al large set of points as rows of a matrix with 2 columns.
Class @Point2D.Stored
//////////////////////////////////////////////////////////////////////////////
{
  VMatrix _.xy;

  Real numberOfPoints(Real void) { VRows(_.xy) };
  Real x(Real k) { VMatDat(_.xy,k,1) };
  Real y(Real k) { VMatDat(_.xy,k,1) };

  @Point2D.Simple center(Real void)
  {
    Real m = VRows(_.xy);
    VMatrix c = Rand(m,1,1/m,1/m)*_.xy;
    @Point2D.Simple::New(VMatDat(c,1,1), VMatDat(c,1,2))
  };

  ////////////////////////////////////////////////////////////////////////////
  Static Text _.autodoc.member.New = "Creates an instance of @Point2D.Stored";
  Static @Point2D.Stored New(VMatrix xy)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(VColumns(xy)>2,
      WriteLn("[@Point2D.Stored::New] Only 2 first of "<<VColumns(xy)+
      " columns of argument xy will be used","W"));
    If(VColumns(xy)<2,
      WriteLn("[@Point2D.Stored::New] Almost 2 columns instead of "<<
      VColumns(xy)+" are needed for argument xy","E"));
    @Point2D.Stored new = [[
      VMatrix _.xy = xy ]];
    new
  }
};
